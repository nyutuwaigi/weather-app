import React from "react";


const Title = () => (
  <div >
      <h1 className="title-container__title">Kifaru Weather Finder</h1>
      <h2 className="title-container__subtitle">Wield the tool, decode the elements</h2>
  </div>
)


export default Title;