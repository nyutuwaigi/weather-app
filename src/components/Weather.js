import React from "react";

class Weather extends React.Component {
    render() {
        return(
            <div>
                { this.props.temperature && <p>Temperature: { this.props.temperature } °F </p> }
                { this.props.country && <p>Country: { this.props.country }, { this.props.city }</p> }
                { this.props.humidity && <p>Humidity: { this.props.humidity}</p> }
                { this.props.description && <p>Conditions: { this.props.description }</p> }
                { this.props.error && <p> { this.props.error }</p>}
            </div>
        );
    }
};

export default Weather;

