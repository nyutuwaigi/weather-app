import React from "react";

class Weather = props => (
        <div className="weather__info">
            { 
                props.temperature && <p>Temperature: { props.temperature }</p> 
            }
            { 
                props.country && <p>Country:
                     <span>{ props.country }, { props.city }</span>
                     </p> 
            }
            { 
                props.humidity && <p>Humidity:
                     <span>{ props.humidity }</span>
                     </p> 
            }
            { 
                props.description && <p>Conditions: 
                <span>{ props.description }</span>
                </p> 
            }
            { 
                props.error && <p> { props.error }</p>
            }
        </div>
    );
}

export default Weather;

